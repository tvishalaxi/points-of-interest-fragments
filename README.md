In this project two Android work together on an Android phone. Here is a short summary of the apps:

#1. Application A1

consists of a single activity containing two read-only text views and two buttons. The
buttons, when selected, will first show a short toast message, then broadcast two different intents (e.g.,
Chicago vs. Indianapolis) depending on the button pressed. The text views describe the meaning of the
buttons to the device user.

#2. Application A2
receives the intents. Depending on the kind of intent that was received, A2will launch
one of two activities. The first activity displays information about 12 points of interest in the city of
Chicago, Illinois (e.g., Millenium Park, the Museum of Science and Indutry, the Art Institute, the Willis
Tower, the Lincoln Park Zoo, etc.) The second activity shows at least 4 points of interest in the city of
Indianapolis, Indiana (e.g., the Indianapolis Motor Speedway, the Children’s Museum). Both activities
consist of two fragments, whose behavior is described below. In addition, application A2
maintains an options menu and an action bar. The action bar shows the name of the application (your choice) and
the overflow area. The options menu allows a device user to switch between cities. This menu should
be clearly accessible from the overflow area.

Each of the two activities in A2 contains two fragments. The first fragment displays a list of points of interest
(scrollable, if necessary). The device user may select any point of interest from the list; the currently selected
item is highlighted. The second fragment shows the official web site of the highlighted item using a browser
stored on the device. This browser could be Firefox, Chrome or any other, depending on installed applications
and the preferences of the device’s user.
When the device is in portrait mode the two fragments are displayed on different screens. First, the device
will show only the first fragment. When the user selects an item, the the first fragment disappears and the
second fragment is shown. Pressing the “back” soft button on the device, will return the device to the original
configuration (first fragment only), thereby allowing the user to select a different point of interest. When the
device is in landscape mode, application A2 initially shows only the first fragment across the entire width
of the screen. As soon as a user selects an item, the first fragment is “shrunk” to about 1/3 of the screen’s
width. This fragment will appear in the left-hand side of the screen, with the second fragment taking up the
remaining 2/3 of the display on the right. Again, pressing the “back” button will return the application to its
initial configuration. The action bar should be displayed at all times regardless of whether the device is in
portrait or landscape mode.

Finally, the state of application A2 is retained across device rotations, e.g., when the device is
switched from landscape to portrait configuration and vice versa. This means that the selected list item (in the
first fragment) and the page displayed in the second fragment will be kept during configuration changes